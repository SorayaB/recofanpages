import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class MySQLConnexionSingleton {
	private static MySQLConnexionSingleton instance = null;
	public Connection connexion;
	private String url, utilisateur, motDePasse;

	private MySQLConnexionSingleton(String url, String utilisateur,
			String motDePasse) {

		this.url = url;
		this.utilisateur = utilisateur;
		this.motDePasse = motDePasse;

	}

	public static MySQLConnexionSingleton getInstance(String url, String utilisateur,
			String motDePasse) {
		if (instance == null) {

			instance = new MySQLConnexionSingleton(url, utilisateur, motDePasse);
			instance.connexionNew();
		}
		return instance;
	}

	private void connexionNew() {
		/* Chargement du driver JDBC pour MySQL */
		try {
			// messages.add( "Chargement du driver..." );
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("      Driver chargé !");
		} catch (ClassNotFoundException e) {
			System.out.println("      Erreur lors du chargement : le driver n'a pas été trouvé dans le classpath ! " + e.getMessage());
		}

		/* Connexion à la base de données */

		Connection connexion = null;

		try {
			System.out.println("      Connexion à la base de données...");
			connexion = (Connection) DriverManager.getConnection(url,
					utilisateur, motDePasse);
			System.out.println("      Connexion réussie !");

			/* Création de l'objet gérant les requêtes */
		} catch (SQLException e) {
			System.out.println("      Erreur lors de la connexion : " + e.getMessage());
		}
		this.connexion = connexion;
	}
	
	public void deconnexion() throws SQLException{
		connexion.close();
		instance = null;
		
	}

}