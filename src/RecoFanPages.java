import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Date;

import com.mysql.jdbc.Connection;


public class RecoFanPages {

// Li��s au code 
	private static String connectSQL = "mysql.connect";
	final static Charset ENCODING = StandardCharsets.UTF_8;
	private static MySQLConnexionSingleton c = null;
	private static Connection connexion = null;

// Li��s �� la reco
	private static int idTypeReco = -1;
	private static String typeReco = "Fan pages";
	private static double currentVersion = 0.1;
	private static int nbRecosGenerees = 5;
	private static int nbJoursAncienete = 60 ;
	private static double penNotClicked = 0.98;	// Penalite a l'affichage
	private static double penClicked = 1.2;		// Penalite au clic

	
// ---------------- CHANGER ICI LA VERSION APPELEE -----------------
	private static void recoFanPageAlgo(int userID) throws SQLException, IOException {
		System.out.println("     * user_id = " + userID);
		recoFanPageV0(userID);
		
	}
// -----------------------------------------------------------------	
	
	
	private static void recoFanPagesAllUser() throws SQLException, IOException {

		connectionBDD();
		getIdTypeReco(typeReco,currentVersion);
				
		java.sql.Statement statement = connexion.createStatement();
		ResultSet resultat = null;
		
		// -- RECUPERATION ID USERS
		/*String requete = "(" +
				"SELECT user_id , MAX(displayed) disp , categorie, reco_date_generation, NOW(), MAX(TIMESTAMPDIFF(HOUR,reco_date_generation, NOW())) duree " +
				"FROM recos R, type_recos TR, reco_categories RC " +
				"WHERE R.type_reco = TR.id AND TR.id_categorie=RC.id " +
				"AND reco_date_generation = (" +
											"SELECT MAX(reco_date_generation) " +
											"FROM recos R2, type_recos TR2, reco_categories RC2 " +
											"WHERE R2.type_reco = TR2.id AND TR2.id_categorie=RC2.id " +
											"AND RC2.categorie = \"" + typeReco + "\" " +
											"AND  R2.user_id = R.user_id" +
											") " +
				"AND categorie = \"" + typeReco + "\" " +
				"GROUP BY user_id " +
				"HAVING disp > 0 OR duree >= 20" +
			") UNION (" +
				"SELECT id user_id, null disp, null categorie, null reco_date_generation, NOW(), null duree " +
				"FROM users " +
				"WHERE id not in (" +
									"select distinct user_id " +
									"from recos R, type_recos TR, reco_categories RC " +
									"where R.type_reco = TR.id and TR.id_categorie=RC.id " +
									"and categorie = \"" + typeReco + "\"))";
		*/
		String requete = "(" +
				"SELECT R.user_id , MAX(displayed) disp , categorie, reco_date_generation, NOW(), MAX(TIMESTAMPDIFF(HOUR,reco_date_generation, NOW())) duree " +
				"FROM recos R, type_recos TR, reco_categories RC, " +
				"(SELECT user_id, MAX(reco_date_generation) max_date FROM recos R2, type_recos TR2, reco_categories RC2 WHERE R2.type_reco = TR2.id AND TR2.id_categorie=RC2.id AND RC2.categorie = \"" + typeReco + "\" GROUP BY R2.user_id) DATEGEN " +
				"WHERE R.type_reco = TR.id AND TR.id_categorie = RC.id " +
				"AND categorie = \"" + typeReco + "\" " +
				"AND R.user_id = DATEGEN.user_id " +
				"AND reco_date_generation = DATEGEN.max_date " +
				"GROUP BY user_id " +
				"HAVING disp > 0 OR duree >= 20" +
			") UNION (" +
				"SELECT id user_id, null disp, null categorie, null reco_date_generation, NOW(), null duree " +
				"FROM users " +
				"WHERE id not in (" +
								"select distinct user_id " +
								"from recos R, type_recos TR, reco_categories RC " +
								"where R.type_reco = TR.id " +
								"and TR.id_categorie=RC.id " +
								"and categorie = \"" + typeReco + "\" " +
								")" +
			")" ;
		
		//System.out.println(requete);

		resultat = statement.executeQuery(requete);
		
		
		// -- APPEL DE LA FONCTION PAR UTILISATEUR
		while(resultat.next()) {
			recoFanPageAlgo(resultat.getInt("user_id"));
		}
		
		deconnectionBDD();
		System.out.println("   Reco multiple terminee !");
	}

	
	private static void recoFanPage1User(int userID) throws SQLException, IOException {
		//Ok
		connectionBDD();
		System.out.println("     getIdTypeReco Commencé");
		getIdTypeReco(typeReco,currentVersion);
		System.out.println("     getIdTypeReco Fini");
		
		System.out.println("     RecoLieux_algo Commencé");
		recoFanPageAlgo(userID);
		System.out.println("     RecoLieux_algo Fini");
		
		System.out.println("     deconnectionBDD");
		deconnectionBDD();
		System.out.println("     Reco 1 user terminee !");
		
	}
	
	
	public static void main(String[] args) throws Exception { doJobs(4);}
	public static void doJobs() throws SQLException, IOException { recoFanPagesAllUser(); }
	public static void doJobs(int userID) throws SQLException, IOException { recoFanPage1User(userID); }
	
	
	private static void recoFanPageV0(int userID) throws SQLException, IOException {
		
		//System.out.println("   * userID : " + userID); 
		
				
		java.text.SimpleDateFormat datef = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		java.sql.Statement statement = connexion.createStatement();
		ResultSet resultFanPages = null;
		
		
		// -- RECUPERATION DES FAN PAGES PAR ORDRE DE PERTINENCE --
		String requeteFanPages = "SELECT name, created_at, 1*POW(" + penNotClicked + ",COALESCE(notClicked_tot,0))*POW(" + penClicked + ",COALESCE(clicked_tot,0)) as interest " ;
		requeteFanPages += "FROM (SELECT DISTINCT user_id, name, created_at FROM fanpages P WHERE P.user_id = " + userID ;
		//requeteFanPages += " AND created_at > ALL (select created_at from recos where user_id = " + userID + ")";
		requeteFanPages += ") F1 LEFT JOIN ";
		requeteFanPages += "(";
		requeteFanPages += "SELECT SUM(displayed)-SUM(clicked) as notClicked_tot, SUM(clicked) as clicked_tot, notes ";
		requeteFanPages += "FROM recos R, type_recos TR, reco_categories RC ";
		requeteFanPages += "WHERE R.type_reco = TR.id AND TR.id_categorie = RC.id ";
		requeteFanPages += "AND categorie = \"Fan pages\" ";
		//requeteFanPages += " ORDER BY created_at DESC;";
		requeteFanPages += "AND user_id = " + userID + " ";
		requeteFanPages += "AND displayed > 0 ";
		requeteFanPages += "GROUP BY notes ";
		requeteFanPages += "HAVING notes in (SELECT DISTINCT name FROM fanpages P WHERE user_id = " + userID + ")";
		requeteFanPages += ") F2 ";
		requeteFanPages += "ON F1.name=F2.notes ";
		//requeteVilles += "WHERE LENGTH(F1.name) < 10 ";
		requeteFanPages += "ORDER BY created_at DESC, interest DESC, RAND() ;";
		//requete += "LIMIT " + nbRecosGenerees +";";
		
		System.out.println("RECUPERATION DES VILLES PAR ORDRE DE PERTINENCE ");
		System.out.println("   requete : " + requeteFanPages);
		
		// -- RECUPERATION DES FAN PAGES PAR ORDRE DE Nombre de Likes --
		// Doublons de fanpages avec nombre de likes différents
		String requeteFanPagesLikes = "SELECT DISTINCT name ";
		requeteFanPagesLikes += "FROM fanpages P ";
		requeteFanPagesLikes += "WHERE P.user_id = " + userID ;
		requeteFanPagesLikes += " ORDER BY likes DESC;";
		 
		// -- RECUPERATION DES nouvelles FAN PAGES PAR ORDRE DE Date d'enregistrement d'une nouvelle fanpage --
		// C'est la date à laquelle notre systéme a enregistré que le user a aimé une nouvelle page
		String requeteFanPagesDate = "SELECT DISTINCT name ";
		requeteFanPagesDate += "FROM fanpages P ";
		requeteFanPagesDate += "WHERE P.user_id = " + userID ;
		requeteFanPagesDate += " AND created_at > ALL (select created_at from recos where user_id = " + userID + ")";
		requeteFanPagesDate += " ORDER BY created_at DESC;";
		
		// -- RECUPERATION DES FAN PAGES PAR ORDRE DE Date de création de la page --
		// Ce n'est pas la date de création de la page mais celle de l'insertion dans la base
		String requeteFanPagesDateCre = "SELECT DISTINCT name ";
		requeteFanPagesDateCre += "FROM fanpages P ";
		requeteFanPagesDateCre += "WHERE P.user_id = " + userID ;
		requeteFanPagesDateCre += " ORDER BY created_at DESC;";
		
		// -- RECUPERATION DES FAN PAGES par ordre des catégories Les plus aimés
		// Corriger le count() group by user
		String requeteFanPagesCat = "SELECT DISTINCT name, P.category, P1.interest as interestInCat ";
		requeteFanPagesCat += "INNER JOin ( ";
		requeteFanPagesCat += "select category, count(id) as interest ";
		requeteFanPagesCat += "FROM fanpages ";
		requeteFanPagesCat += "group by category) P1 ";
		requeteFanPagesCat += "ON P.category = P1.category ";
		requeteFanPagesCat += "WHERE P.user_id = " + userID ;
		requeteFanPagesCat += " ORDER BY P1.interest DESC ;";
		System.out.println(requeteFanPagesCat);
		
		// Enlever fan pages de villes
		// Enlever doublons fanpages
		
				
		resultFanPages = statement.executeQuery(requeteFanPages);
		
		// -- RECUPERATION DES ARTICLES ELIGIBLES
		java.sql.Statement statement2 = connexion.createStatement();
		ResultSet resultatArticlesRecentsDejaAffiches = null;
		// Changement du timestamp pour test
		String requeteArticlesRecentsDejaAffiches =	"SELECT DISTINCT HL.id " +
													"FROM home_links HL, recos R " +
													"WHERE HL.id = R.article_id " +
													"AND user_id = " + userID + " " +
													"AND TIMESTAMPDIFF(DAY,datePost, NOW()) <= 30 * " + nbJoursAncienete + " " +
													"AND displayed > 0 ;" ;
		
		System.out.println("RECUPERATION DES Fan pages ELLIGIBLES ");
		System.out.println("   requete : " + requeteArticlesRecentsDejaAffiches);
		
		
		resultatArticlesRecentsDejaAffiches = statement2.executeQuery(requeteArticlesRecentsDejaAffiches);
		ArrayList<Integer> idsArticlesRecentsDejaAffiches = new ArrayList<Integer>();
		while(resultatArticlesRecentsDejaAffiches.next())
			idsArticlesRecentsDejaAffiches.add(resultatArticlesRecentsDejaAffiches.getInt("id"));
		
		
		// -- RECUPERATION DES ARTICLES PERTINENTS --
		ArrayList<String> fanPages = new ArrayList<String>();
		ArrayList<double[][]> articlesProches = new ArrayList<double[][]>();	// articlesProches.get(idxville)[idxsuggestion][0:idarticle;1:score]
		//int nbVilles = 0 ;
		double[][] artprx;
		String fanPage;
		while(resultFanPages.next()) {
			// A changer pour Fan pages
			fanPage = resultFanPages.getString("name");
			artprx = IndexReaderMat.getNearestArticles("\"" + fanPage + "\"" , nbRecosGenerees , nbJoursAncienete , idsArticlesRecentsDejaAffiches);
			if (artprx.length > 0) { // On ne garde que les fan pages qui ont au moins une suggestion
				fanPages.add(fanPage);
				articlesProches.add(artprx);
				//nbVilles++;
			}
		}
		
		if (fanPages.size() > 0) { // Il y a au moins une fan page avec des suggestions pour cet utilisateur
		
			// -- PREPARATION REQUETE INSERTION --
			String ReqInsert = "INSERT INTO recos(user_id, article_id, displayed, clicked, reco_rank, reco_date_generation, type_reco, score, notes, created_at,updated_at) VALUES ";
			System.out.println(ReqInsert);
			
			int rk=0 , artiID;
			double score;
			int currentIdxFanPage = 0, currentIdxProx = 0;
			Date dateNow = new Date();
			int seuilTopFanPages = Math.min(nbRecosGenerees,fanPages.size());

			while (rk < nbRecosGenerees) {
				
				if (currentIdxProx < articlesProches.get(currentIdxFanPage).length) { // Si la fan page a assez de suggestions
					rk++;
					artiID = (int)(articlesProches.get(currentIdxFanPage)[currentIdxProx][0]);
					score = articlesProches.get(currentIdxFanPage)[currentIdxProx][1];
					fanPage = fanPages.get(currentIdxFanPage);
					ReqInsert += "( " + userID + " , " + artiID + " , 0 , 0 , " + rk + " , '" + datef.format(dateNow) + "' , " + idTypeReco + " , " + score + " , '" + fanPage.replace("'", " ") + "' , '" + datef.format(dateNow) + "' , '" + datef.format(dateNow) + "'), ";
	
				}
				
				currentIdxFanPage++;
				
				if (currentIdxFanPage == seuilTopFanPages){ // Si on est arriv�� �� la fin du top des villes int��ressantes
					currentIdxFanPage = 0;
					currentIdxProx++;
					if (currentIdxProx == nbRecosGenerees) // Si on a ��puis�� toutes les suggestions
						break;
				}
			}
			
			ReqInsert = ReqInsert.substring(0,ReqInsert.length()-2);
			ReqInsert += ";";
			//ReqInsert.replace("'", "\\\\'");
			
			//System.out.println("   ReqInsert : " + ReqInsert);
			
			
			// -- EXECUTION REQUETE INSERTION
			PreparedStatement pstatement = connexion.prepareStatement(ReqInsert);
			pstatement.executeUpdate();
			System.out.println(ReqInsert);
		}
		else { // Aucune ville avec des suggestions pour cet utilisateur
			System.out.println("     Aucune fan page avec des suggestions pour cet utilisateur");
		}
		
		
	} // fin fonction RecoLieux_0
	
	
	
	private static void connectionBDD() throws IOException {
		ArrayList<String> sMysqlConnect = LireMysqlConnect(connectSQL);
		String sqlUrl = sMysqlConnect.get(0);
        String sqlUtilisateur = sMysqlConnect.get(1);
        String sqlmotDePasse = sMysqlConnect.get(2);
		
		c = MySQLConnexionSingleton.getInstance(sqlUrl,	sqlUtilisateur, sqlmotDePasse);
		connexion = c.connexion;
	}
	
	private static void deconnectionBDD() throws SQLException {
		c.deconnexion();
	}
	
	private static ArrayList<String> LireMysqlConnect(String filename) throws IOException {
		List<String> lines = readTextFile(filename);
		ArrayList<String> res = new ArrayList<String>();
		for (String s : lines) {
			String part[] = s.split("::");
			if (part.length > 1) {
				res.add(part[1]);
			} else {
				res.add("");
			}

		}

		return res;
	}
	
	private static List<String> readTextFile(String aFileName) throws IOException {
		Path path = Paths.get(aFileName);
		return Files.readAllLines(path, ENCODING);
	}
	
	private static void getIdTypeReco(String categorie, double version) throws SQLException {

		String requete = "SELECT TR.id as TRid ";
		requete += "FROM type_recos TR, reco_categories RC ";
		requete += "WHERE TR.id_categorie = RC.id ";
		requete += "AND categorie = '" + categorie + "' ";
		requete += "AND version = " + version + " ;";
		
		java.sql.Statement statement = connexion.createStatement();
		ResultSet resultat = statement.executeQuery(requete);
		
		resultat.next();
		try {
			idTypeReco = resultat.getInt("TRid");
			System.out.println("idTypeReco "+ idTypeReco);
		}
		catch (Exception e) {
			idTypeReco = 0;
		}

	}
	
	
} // fin classe