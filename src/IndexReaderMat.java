import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class IndexReaderMat {

	static String indexPath = "index";
	public static final String ARTICLETEXT = "ArticleText";
	public static final String ARTICLENAME = "name";
	static String csvMatrix = "Matrix.csv";

	
	public static void main(String[] args) throws IOException { getNearestArticles("Paris", 5,1,null);}
	
	public static double[][] getNearestArticles(String keyWords, int numberSearch, int days, ArrayList<Integer> idsAEliminer) throws IOException {
		
		String queryComplete = keyWords;
		if (days >= 0) {
			java.util.GregorianCalendar calendar = new java.util.GregorianCalendar(); 
			java.util.Date today = calendar.getTime();
			calendar.add(Calendar.DATE, -1 * days);
			java.util.Date previous = calendar.getTime();
			java.text.DateFormat dateFormatPerso = new java.text.SimpleDateFormat("yyyyMMdd");
			String todayS = dateFormatPerso.format(today);
			String previousS = dateFormatPerso.format(previous);
			
			String queryDate = " AND date:[" + previousS + " TO " + todayS + "]";
			queryComplete += queryDate;
		}
		if (idsAEliminer != null) {
			String queryIds = "";
			for (Integer i : idsAEliminer)
				queryIds += " AND NOT id:" + i;
			queryComplete += queryIds;
		}
		//System.out.println(queryComplete);
		
		IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(indexPath)));
		TopDocs results = indexSearch(queryComplete, reader, numberSearch);
		
		ScoreDoc[] hits = results.scoreDocs;
		int numTotalHits = results.totalHits;
System.out.println("     numTotalHits (" + keyWords +") : " + numTotalHits);
		int end = Math.min(numTotalHits, numberSearch);
		
		double[][] result = new double[end][2];
		double idLucene, idArticle, score;
		for(int i = 0;i<end;i++){
			
			idLucene = hits[i].doc;
			idArticle = Double.parseDouble(reader.document(hits[i].doc).getField("id").stringValue());
			score = hits[i].score;
			
			result[i][0] = idArticle;
			result[i][1] = score;
		
			//System.out.println("idLucene="+idLucene+"idArticle="+idArticle+" score="+score);
			
		} // */
		
		reader.close();
		
		return result;
	}

	public static TopDocs indexSearch(String textquery, IndexReader reader, int numberSearch){
		// Permet de récupérer les documents les plus proches
		
		TopDocs results = null;
		
		IndexSearcher searcher = new IndexSearcher(reader);
		Analyzer analyzer = new FrenchAnalyzer(Version.LUCENE_46);
		QueryParser parser = new QueryParser(Version.LUCENE_46, ARTICLETEXT, analyzer);
		
		try {
			Query query = parser.parse(textquery);
			results = searcher.search(query, numberSearch);
			
			
			
		} catch (ParseException e) {
			// TODO Bloc catch généré automatiquement
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Bloc catch généré automatiquement
			e.printStackTrace();
		}
		
		
		return results;
	}
	
	
	static void indexLecture(IndexReader reader) throws IOException {
		//Crée un fichier csv avec tous les documents et mots de l'index
		
		int max = reader.maxDoc();
		System.out.println("max : " + max);
		TermsEnum term = null;
		// iterate docs
		HashSet<String> termSpace = new HashSet<String>();
		
		ArrayList<ArrayList<String>> matrixTerme = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<Long>> matrixFreq = new ArrayList<ArrayList<Long>>();
		//HashMap<String, Long> vecteurTerme = new HashMap<String,Long>();
		
		
		for (int i = 0; i < max; ++i) {
			// get term vector for body field
			final Terms terms = reader.getTermVector(i, ARTICLETEXT);
			ArrayList<Long> freq = new ArrayList<Long>();
			ArrayList<String> vecteurTerme = new ArrayList<String>();
			freq.clear();
			vecteurTerme.clear();
			if (terms != null) {
				int k=0;
				term = terms.iterator(term);
				while (term.next() != null) {
					
					termSpace.add(term.term().utf8ToString());
					vecteurTerme.add(term.term().utf8ToString());
					freq.add(term.totalTermFreq());
				}
				
			}
			matrixTerme.add(vecteurTerme);
			matrixFreq.add(freq);
			for(int f = 0 ; f < vecteurTerme.size();++f){
				System.out.println(i + " / " + vecteurTerme.get(f)+ " - " + freq.get(f));
			}
			
			
		}
		long[][]matrix =new long[max][termSpace.size()]; 
		System.out.println("nb mots: " + termSpace.size());
		
		FileWriter writer = new FileWriter(csvMatrix);
		
		int j = 0;
		for (String s:termSpace){
			writer.append(s + '\t');
			for (int i = 0 ; i < max ; ++i){
				int ind = matrixTerme.get(i).indexOf(s);
				if(ind>-1){
					System.out.println(ind);
					matrix[i][j]=matrixFreq.get(i).get(ind);
				}
			}
			++j;
		}
		writer.append('\n');
		
		
		
		
		for(int i = 0 ; i < max ;++i){
			for(int k = 0; k < termSpace.size() ; ++k){
				writer.append("" + matrix[i][k] + '\t');
			}
			writer.append('\n');
			
		}
		
		writer.flush();
		writer.close();
		
		
		for (int i = 0; i < max; ++i) {
			// get term vector for body field
			final Terms terms = reader.getTermVector(i, ARTICLETEXT);

			

			if (terms != null) {
				// count terms in doc
				int numTerms = 0;
				term = terms.iterator(term);
				while (term.next() != null) {
					
					
					System.out.println("doc " + i + " - term '"
							+ term.term().utf8ToString() + "' "
							+ term.totalTermFreq());
					++numTerms;
				}
				System.out.println("doc " + i + " - " + numTerms + " terms");

			} else {
				System.err.println("doc " + i + " - aucun");
			}

		}

	}
}